module.exports = {
    extractUsername: function (authentication) {
        const result = /username\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractRealm: function (authentication) {
        const result = /realm\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractNonce: function (authentication) {
        const result = /nonce\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractUri: function (authentication) {
        const result = /uri\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractAlgorithm: function (authentication) {
        const result = /algorithm\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractQualityOfProtection: function (authentication) {
        const result = /qop\s*=\s*(auth-int|auth)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractNonceCount: function (authentication) {
        const result = /nc\s*=\s*([0-9A-F]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractClientNonce: function (authentication) {
        const result = /cnonce\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractResponse: function (authentication) {
        const result = /response\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    },

    extractOpaque: function (authentication) {
        const result = /opaque\s*=\s*"([^"]*)/i.exec(authentication);

        if (result) {
            return result[1];
        }
    }
};