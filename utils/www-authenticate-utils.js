module.exports = {

    getNonceGenerator: function () {
        const usedNonceValues = {};

        return {
            getNonce: function () {
                const timestamp = Date.now() + "";
                const buffer = Buffer.from(timestamp);
                const nonce = buffer.toString("base64");

                usedNonceValues[nonce] = true;

                return nonce;
            },

            isNonceInvalid: function (nonce, nonceCount) {
                if (usedNonceValues[nonce]) {
                    delete usedNonceValues[nonce];

                    return nonceCount !== "00000001";
                }
                return true;
            }
        };
    },

    getOpaque: function (realm = "Global") {
        const buffer = Buffer.from(realm);
        const opaque = buffer.toString("base64");

        return opaque;
    }
};