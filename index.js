const express = require("express");
const cors = require("cors");
const md5 = require("md5");
const wwwAuthenticateUtils = require("./utils/www-authenticate-utils");
const authorizationUtils = require("./utils/authorization-utils");

const port = process.env.PORT || 8000;
const app = express();

const realm = "Global";
const qualityOfProtection = "auth";

const nonceGenerator = wwwAuthenticateUtils.getNonceGenerator();

app.use(cors());

app.use(function (request, response, next) {
    const authorization = request.headers["authorization"];
    const regexResult = /digest (.*)/i.exec(authorization);

    if (!regexResult) {
        const nonce = nonceGenerator.getNonce();
        const opaque = wwwAuthenticateUtils.getOpaque(realm);

        response.setHeader("WWW-Authenticate", `Digest realm="${realm}", nonce="${nonce}", opaque="${opaque}", qop="${qualityOfProtection}"`);
        return response.sendStatus(401);
    }

    const authorizationParameters = regexResult[1];
    const receivedNonce = authorizationUtils.extractNonce(authorizationParameters);
    const nonceCount = authorizationUtils.extractNonceCount(authorizationParameters);

    if (nonceGenerator.isNonceInvalid(receivedNonce, nonceCount)) {
        const nonce = nonceGenerator.getNonce();
        const opaque = wwwAuthenticateUtils.getOpaque(realm);

        response.setHeader("WWW-Authenticate", `Digest realm="${realm}", nonce="${nonce}", opaque="${opaque}", qop="${qualityOfProtection}", stale="true"`);
        return response.sendStatus(401);
    }

    const clientResponse = authorizationUtils.extractResponse(authorizationParameters);
    const reconstructedResponse = getReconstructedResponse(authorizationParameters, request.method);

    if (clientResponse !== reconstructedResponse) {
        return response.sendStatus(403);
    }

    next();
});

app.get("", function (request, response) {
    response.sendStatus(200);
});

app.listen(port, function () {
    console.log("Listening on port " + port);
});

function getReconstructedResponse(authorizationParameters, httpMethod) {
    const username = authorizationUtils.extractUsername(authorizationParameters);
    const realm = authorizationUtils.extractRealm(authorizationParameters);
    const password = getPassword(username);
    const h1 = md5(`${username}:${realm}:${password}`);

    const uri = authorizationUtils.extractUri(authorizationParameters);
    const h2 = md5(`${httpMethod}:${uri}`);

    const nonce = authorizationUtils.extractNonce(authorizationParameters);
    const nonceCount = authorizationUtils.extractNonceCount(authorizationParameters);
    const clientNonce = authorizationUtils.extractClientNonce(authorizationParameters);
    const qualityOfProtection = authorizationUtils.extractQualityOfProtection(authorizationParameters);

    let reconstructedResponse;
    if (qualityOfProtection) {
        reconstructedResponse = md5(`${h1}:${nonce}:${nonceCount}:${clientNonce}:${qualityOfProtection}:${h2}`);
    } else {
        reconstructedResponse = md5(`${h1}:${nonce}:${h2}`);
    }

    return reconstructedResponse;
}

function getPassword(username) {
    switch (username) {
        case "user":
            return "pass";
    }
}